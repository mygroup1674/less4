# JuneWay lesson1.4

Текущий Dockerfile создает image с дефолтныйм .conf файлом, но уже содержит в себе кастомную .html страницу.
В репозитории находится конфиругационный файл nginx.conf.
Чтобы добавить конфиг при запуске контейнера на основе образа, получаенного этим Dockerfile, необходимо воспользоваться ключем ```-v```

**Пример команды запуска контейнера №1**

```docker run -itd -v /path/to/file/on/host/nginx.conf:/usr/local/nginx/conf/nginx.conf imageName:latest```

Чтобы зайти в контейнер и проверить, что на нем действительно запущен nginx с данным конфигом:

```docker exec -it idContainer /bin/bash```

Команда: 

```curl 127.0.0.1```

Вывод: \< h1 \> HELLO \< \/h1 \>


**Пример команды запуска контейнера №2**

```docker run -itd -v /path/to/file/on/host/nginx.conf:/usr/local/nginx/conf/nginx.conf -p 8888:80 less4:latest```

Затем в браузере перейти: http://localhost:8888/

Вывод: <h1>HELLO</h1>

_Если раскоментировать строку 28 в Dockerfile и собирать образ самостоятельно, то необходимо при сборке положить рядом с Dockerfile index.html и nginx.conf (можно кастомные)_